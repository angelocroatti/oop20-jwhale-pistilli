package jwhale.model.engine;

public interface Executable {

    Call buildCall();

}
