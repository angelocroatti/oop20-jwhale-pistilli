package jwhale.model.engine;

/**
 * HTTP method types. It's used to perform internal logic.
 *
 */
public enum Method {
    /**
     * GET HTTP method.
     */
    GET,
    /**
     * HEAD HTTP method.
     */
    HEAD,
    /**
     * POST HTTP method.
     */
    POST,
    /**
     * PUT HTTP method.
     */
    PUT,
    /**
     * DELETE HTTP method.
     */
    DELETE
}
